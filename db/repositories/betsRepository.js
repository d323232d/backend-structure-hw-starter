const { BaseRepository } = require('./baseRepository');

class BetsRepository extends BaseRepository {
  constructor() {
    super('bet');
  }
}

module.exports = new BetsRepository();