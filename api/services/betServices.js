const betsRepository  = require('../../db/repositories/betsRepository');

class BetService {
  createBet(payload) {
    return betsRepository.create(payload).then(bet => bet);
  }
  
}

module.exports = new BetService();
