const express = require("express");
const ee = require('events');
const db = require('./db/connection');

const app = express();

const port = 3000;

const statEmitter = new ee();
let stats = {
  totalUsers: 3,
  totalBets: 1,
  totalEvents: 1,
};

const router = require("./api/routes/index");
const { checkRole } = require("./api/middleware/checkRole.middleware");

app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  db.raw('select 1+1 as result').then(function () {
    neededNext();
  }).catch(() => {
    throw new Error('No db connection');
  });
});

router(app);

app.get("/stats", checkRole, (req, res) => {
  try {
    res.send(stats);
  } catch (err) {
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
  }
});

app.listen(port, () => {
  statEmitter.on('newUser', () => {
    stats.totalUsers++;
  });
  statEmitter.on('newBet', () => {
    stats.totalBets++;
  });
  statEmitter.on('newEvent', () => {
    stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };