const { BaseRepository } = require('./baseRepository');

class TransactionsRepository extends BaseRepository {
  constructor() {
    super('transaction');
  }
}

module.exports = new TransactionsRepository();