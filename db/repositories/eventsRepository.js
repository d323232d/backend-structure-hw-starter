const { BaseRepository } = require('./baseRepository');

class EventsRepository extends BaseRepository {
  constructor() {
    super('event');
  }
}

module.exports = new EventsRepository();