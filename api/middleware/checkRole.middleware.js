const jwt = require("jsonwebtoken");

const checkRole = (req, res, next) => {  
  let token = req.headers['authorization'];
    if(!token) {
      return res.status(401).send({ error: 'Not Authorized' });
    }
    token = token.replace('Bearer ', '');
    try {
      const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      if (tokenPayload.type != 'admin') {
        throw new Error();
      }
      req.tokenPayload = tokenPayload;
    } catch (err) {
      return res.status(401).send({ error: 'Not Authorized' });
    }

  next();
};

module.exports = { checkRole };