const transactionsRepository = require('../../db/repositories/transactionsRepository');

class TransactionsServices {
  createTransaction(payload) {
    return transactionsRepository.create(payload).then(transaction => transaction);
  }

  search(search) {
    return transactionsRepository.getOne(search).then(transaction => transaction);
  }

  updateTransaction(id, payload) {
    return transactionsRepository.update(id, payload).then(transaction => transaction);
  }

};

module.exports = new TransactionsServices();