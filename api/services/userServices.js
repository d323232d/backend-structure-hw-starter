const usersRepository = require('../../db/repositories/usersRepository');

class UserService {
  createUser(payload) {
    return usersRepository.create(payload).then(user => user);
  }

  search(search) {
    return usersRepository.getOne(search).then(user => user);
  }

  updateUser(id, payload) {
    return usersRepository.update(id, payload).then(user => user);
  }

}

module.exports = new UserService();