const { BaseRepository } = require('./baseRepository');

class UsersRepository extends BaseRepository {
  constructor() {
    super('user');
  }
}

module.exports = new UsersRepository();