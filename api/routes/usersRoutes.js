const { Router } = require('express');
const { auth } = require('../middleware/auth.middleware');
const { getUserValid, postUserValid, putUserValid } = require('../middleware/userValidation.middleware');
const jwt = require("jsonwebtoken");

const userServices = require('../services/userServices');

const router = Router();

router.get("/:id", getUserValid, (req, res) => {
  try {
    userServices.search(req.params.id).then(([result]) => {
      if (!result) {
        res.status(404).send({ error: 'User not found' });
        return;
      }
      return res.send({
        ...result,
      });
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
  }
});

router.post("/", postUserValid, (req, res) => {
  req.body.balance = 0;
  userServices.createUser(req.body).then((result) => {
    result.createdAt = result.created_at;
    delete result.created_at;
    result.updatedAt = result.updated_at;
    delete result.updated_at;
    return res.send({
      ...result,
      accessToken: jwt.sign({ id: result.id, type: result.type }, process.env.JWT_SECRET),
    });
  })
    .catch((err) => {
      if (err.code == '23505') {
        res.status(400).send({
          error: err.detail,
        });
        return;
      }
      res.status(500).send('Internal Server Error');
      return;
    });

});

router.put("/:id", auth, putUserValid, (req, res) => {
  const tokenPayload = req.tokenPayload;
  if (req.params.id !== tokenPayload.id) {
    return res.status(401).send({ error: 'UserId mismatch' });
  }
  userServices.updateUser(req.params.id, req.body).then(([result]) => {
    return res.send({
      ...result,
    });
  }).catch(err => {
    if (err.code == '23505') {
      console.log(err);
      res.status(400).send({
        error: err.detail
      });
      return;
    }
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
  });
});

module.exports = router;