const db = require('./../connection');

class BaseRepository {
  constructor(tableName) {
    this.tableName = tableName
  }

  getAll() {
    db.raw('select 1+1 as result').then(function () {
      neededNext();
    }).catch(() => {
      throw new Error('No db connection');
    });
    return db(this.tableName)
  }

  create(payload) {
    return db(this.tableName).insert(payload).returning("*")
  }

  getOne(search) {
    return db(this.tableName).where('id', search).returning("*")
  }

  update(id, payload) {
    return db(this.tableName).where('id', id).update(payload).returning("*")
  }
};

module.exports = { BaseRepository };

