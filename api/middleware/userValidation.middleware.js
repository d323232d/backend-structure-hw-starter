const joi = require('joi');

const getUserValid = (req, res, next) => {
  const schema = joi.object({
    id: joi.string().uuid(),
  }).required();
  const isValidResult = schema.validate(req.params);
  if(isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };
  next();
};

const postUserValid = (req, res, next) => {
  const schema = joi.object({
    id: joi.string().uuid(),
    type: joi.string().required(),
    email: joi.string().email().required(),
    phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/).required(),
    name: joi.string().required(),
    city: joi.string(),
  }).required();
  const isValidResult = schema.validate(req.body);
  if(isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };
  next();
};

const putUserValid = (req, res, next) => {
  const schema = joi.object({
    email: joi.string().email(),
    phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
    name: joi.string(),
    city: joi.string(),
  }).required();
  const isValidResult = schema.validate(req.body);
  if(isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };
  next();
};

module.exports = {
  getUserValid,
  postUserValid,
  putUserValid
}