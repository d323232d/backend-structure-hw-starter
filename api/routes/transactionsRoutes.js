const { Router } = require('express');

const { checkRole } = require('../middleware/checkRole.middleware');
const { transactionsValid } = require('../middleware/transactionsValidation.middleware');
const transactionServices = require('../services/transactionServices');
const userServices = require('../services/userServices');

const router = Router();

router.post("/", checkRole, transactionsValid, (req, res) => {
  userServices.search(req.body.userId).then(([user]) => {
    if (!user) {
      res.status(400).send({ error: 'User does not exist' });
      return;
    }
    req.body.card_number = req.body.cardNumber;
    delete req.body.cardNumber;
    req.body.user_id = req.body.userId;
    delete req.body.userId;
    transactionServices.createTransaction(req.body).then(([result]) => {
      let currentBalance = req.body.amount + user.balance;
      userService.updateUser(req.body.userId, { balance: currentBalance }).then(() => {
        ['user_id', 'card_number', 'created_at', 'updated_at'].forEach(whatakey => {
          let index = whatakey.indexOf('_');
          let newKey = whatakey.replace('_', '');
          newKey = newKey.split('')
          newKey[index] = newKey[index].toUpperCase();
          newKey = newKey.join('');
          result[newKey] = result[whatakey];
          delete result[whatakey];
        })
        return res.send({
          ...result,
          currentBalance,
        });
      });
    });
  }).catch(err => {
    res.status(500).send("Internal Server Error");
    return;
  });

});

module.exports = router;