const jwt = require("jsonwebtoken");

const auth = (req, res, next) => {  
  let token = req.headers['authorization'];
  let tokenPayload;  
  if (!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  
  token = token.replace('Bearer ', '');  
  
  try {
    tokenPayload = jwt.verify(token, process.env.JWT_SECRET);    
    req.tokenPayload = tokenPayload;
  } catch (err) {
    return res.status(401).send({ error: 'Not Authorized' });
  }

  next();
};

module.exports = { auth };