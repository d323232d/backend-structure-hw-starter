const { checkRole } = require("../middleware/checkRole.middleware");


app.get("/stats", checkRole, (req, res) => {
  try {
    res.send(stats);
  } catch (err) {
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
  }
});